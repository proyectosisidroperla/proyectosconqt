#include "factorajustedevalor.h"
#include "ui_factorajustedevalor.h"
#include <QItemDelegate>
#include <qdebug.h>
#include <opciones.h>

int opc5;
QString nombre_bd5;

int suma=0;

QString car[15][2] ={
    "Respaldo y recuperación", "0", "Comunciaciones de datos", "0", "Procesamiento distribuido", "0", "Rendimiento crítico", "0",
    "Existencia de entorno operativo", "0", "Entrada de datos en línea", "0", "Transacción de entrada sobre múltiples pantallas", "0", "Archivos maestros actualizados en línea", "0",
    "Complejo de valores de dominio de información", "0", "Complejo de procesamiento interno", "0", "Código diseñado para reuso", "0", "Conversión/instalación en diseño", "0", "Instalaciones múltiples", "0",
    "Aplicación diseñada para cambio", "0", "Total", "0"
};


class Delegate : public QItemDelegate //Clase para validar solo la entrada de números en la tablewidget.
{
public:
    QWidget* createEditor(QWidget *parent, const QStyleOptionViewItem & option,
                      const QModelIndex & index) const
    {
        QLineEdit *lineEdit = new QLineEdit(parent);
        //Set validator
        QIntValidator *validator = new QIntValidator(0, 5, lineEdit);
        lineEdit->setValidator(validator);
        return lineEdit;
    }
};

int contarFilas(){
    QSqlQuery q;
    q.prepare("SELECT COUNT (*) FROM FAV");
    q.exec();
    int elementos= 0;
    if (q.next()) {
        elementos= q.value(0).toInt();
    }
    return elementos;
}

FactorAjusteDeValor::FactorAjusteDeValor(int opc, QString nombre_bd, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::FactorAjusteDeValor)
{
    ui->setupUi(this);

    opc5 = opc;
    nombre_bd5 = nombre_bd;

    setWindowFlags(windowFlags() | Qt::CustomizeWindowHint |
                                   Qt::WindowMinimizeButtonHint |
                                   Qt::WindowMaximizeButtonHint |
                                   Qt::WindowCloseButtonHint);

    ui->lineEdit->hide();
    ui->tableWidget->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    ui->tableWidget->setItemDelegate(new Delegate); //Validar solo entrada de numeros.

    ui->tableWidget->setStyleSheet("background-color: rgb(173, 181, 189);"
                               "QHeaderView::section { color:white; background-color:#232326; }");

    dbmain = QSqlDatabase::addDatabase("QSQLITE");
    dbmain.setDatabaseName(nombre_bd5);
    if(dbmain.open()){
        qDebug() << "Base de datos creada";
    }else{
        qDebug() << "Error";
    }

    CrearTabla();

    if(contarFilas()<=7){

        QSqlQuery insertar_db;
        insertar_db.prepare("INSERT INTO FAV(caracteristicas,valor,total)"
                            "VALUES (:ctrs,:val,:tol)");

    for (int i=0; i<15; i++)
    {
        for(int j=0; j<2-1; j++){

            insertar_db.bindValue(":ctrs", car[i][j]);
            insertar_db.bindValue(":val", "0");

            if(insertar_db.exec()){
                //qDebug() << "Datos ingresados a la tabla";
            }else{
                //qDebug() << "Error al ingresar los datos" << insertar_db.lastError();
            }
        }
    }
    }

    QString consulta;
    consulta.append("SELECT * FROM FAV");
    QSqlQuery consultar;
    consultar.prepare(consulta);

    if(consultar.exec()){
        qDebug() << "Se ha consultado correctamente";
    }else{
        qDebug() << "No se ha consultado correctamente" << consultar.lastError();
    }

    for(int i=0; i<15;i++){
        if(consultar.next()){
        ui->tableWidget->insertRow(i);
        ui->tableWidget->setItem(i, 0, new QTableWidgetItem(consultar.value(1).toByteArray().constData()));
        ui->tableWidget->setItem(i, 1, new QTableWidgetItem(consultar.value(2).toByteArray().constData()));
        }
    }

    if(opc5==2){
        ModalidadEjemplo();
    }
}

FactorAjusteDeValor::~FactorAjusteDeValor()
{
    delete ui;
}

void FactorAjusteDeValor::ModalidadEjemplo(){

    QFont f( "Times New Roman", 10, QFont::Bold);
    ui->lb_Demo->setFont( f);

    ui->lb_Demo->setText("<center>MODO DEMOSTRACIÓN (NO ES EDITABLE)</center> \nAquí se le debe asignar un nivel de importancia que va del 0 al 5, a cada uno de los 14"
                         " factores mostrados. La suma de estos puntos se usará para determinar el Factor de Ajuste de Valor.");
    ui->lb_Demo->setWordWrap(true);

    ui->btn_Actualizar->setEnabled(false);
    ui->tableWidget->setEditTriggers(QAbstractItemView::NoEditTriggers);

}

void FactorAjusteDeValor::CrearTabla(){ //Creación de la tabla
    QString consulta;
    consulta.append("CREATE TABLE IF NOT EXISTS FAV("
                    "id INTEGER PRIMARY KEY AUTOINCREMENT,"
                    "caracteristicas VARCHAR(50),"
                    "valor VARCHAR (10),"
                    "total VARCHAR (10)"

                    ");");

    QSqlQuery crear;
    crear.prepare(consulta);

    if(crear.exec()){
        //qDebug() << "Tabla creada";
    }else{
        //qDebug() << "Tabla no creada" << crear.lastError();
    }

}

void FactorAjusteDeValor::on_tableWidget_cellChanged(int row, int column)
{
    QTableWidgetItem* theItem = ui->tableWidget->item(row, column);
    QString decrip = theItem->text();

    QSqlQuery actualizar;
    actualizar.prepare("UPDATE FAV SET valor = :val WHERE id='" + QString::number(row+1) + "'");
    actualizar.bindValue(":val", decrip);
    actualizar.exec();

    if(actualizar.exec()){
        qDebug() << "Se ha actualizado correctamente";
    }else{
        qDebug() << "No se ha actualizado correctamente" << actualizar.lastError();
    }

}

void FactorAjusteDeValor::on_btn_Actualizar_clicked()
{
    for(int i=0; i<14; i++){
        QTableWidgetItem* theItem = ui->tableWidget->item(i, 1);
        suma += theItem->text().toInt();
    }
    ui->tableWidget->item(14,1)->setText(QString::number(suma));
    QSqlQuery actualizar;
    actualizar.prepare("UPDATE FAV SET valor = :val WHERE id='15'");
    actualizar.bindValue(":val", QString::number(suma));
    actualizar.exec();
    suma=0;

}

void FactorAjusteDeValor::on_toolButton_clicked()
{
    this->close(); //Se cierra esta ventana.

    Opciones *ops;
    ops = new Opciones(opc5, nombre_bd5, this);
    ops->show(); //Se abre la ventana para la simulación con varios objetos.
}
