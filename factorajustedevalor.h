#ifndef FACTORAJUSTEDEVALOR_H
#define FACTORAJUSTEDEVALOR_H

#include <QDialog>
#include "QtSql/QSqlDatabase";
#include "QtSql/qsqlquery.h";
#include "QtSql/QSqlError";
#include "QtSql/QSqlQuery";

namespace Ui {
class FactorAjusteDeValor;
}

class FactorAjusteDeValor : public QDialog
{
    Q_OBJECT

public:
    explicit FactorAjusteDeValor(int opc, QString nombre_bd, QWidget *parent = nullptr);
    ~FactorAjusteDeValor();
    void CrearTabla();
    void actualizar2();
    void ModalidadEjemplo();

private slots:
    void on_btn_Actualizar_clicked();

    void on_tableWidget_cellChanged(int row, int column);



    void on_toolButton_clicked();

private:
    Ui::FactorAjusteDeValor *ui;
    QSqlDatabase dbmain;
};

#endif // FACTORAJUSTEDEVALOR_H
