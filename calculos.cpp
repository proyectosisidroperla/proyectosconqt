#include "calculos.h"
#include "ui_calculos.h"
#include <QDebug>
#include <opciones.h>
#include <qpdfwriter.h>
#include <qpainter.h>
#include <qpdfwriter.h>
#include <qpainter.h>
#include <qfiledialog.h>
#include <qdatetime.h>
#include <QMessageBox>

int opc6;
QString nombre_bd6;

float PFsA=0;
float sigma_Fi=0;
float PFA=0;
float numPersonas;
float horasLaborales;
float horasLaboralesMes;

float esfuerzo=0;
float duracionHoras;
float duracionMeses;

QString complejidad;

Calculos::Calculos(int opc, QString nombre_bd, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Calculos)
{
    ui->setupUi(this);

    opc6 = opc;
    nombre_bd6 = nombre_bd;

    setWindowFlags(windowFlags() | Qt::CustomizeWindowHint |
                                   Qt::WindowMinimizeButtonHint |
                                   Qt::WindowMaximizeButtonHint |
                                   Qt::WindowCloseButtonHint);

    ui->dSpin_Horas->setRange(0,24);
    ui->dSpin_HorasMes->setRange(0,300);

    dbmain = QSqlDatabase::addDatabase("QSQLITE");
    dbmain.setDatabaseName(nombre_bd6);
    if(dbmain.open()){
        qDebug() << "Base de datos creada";
    }else{
        qDebug() << "Error";
    }

    QSqlQuery q;

    q.prepare("SELECT * FROM conteo_complejidad WHERE id='6'");
    q.exec();
    QString FA;
    if (q.next()) {
        FA = q.value(6).toByteArray().constData();
    }

    q.prepare("SELECT * FROM FAV WHERE id='15'");
    q.exec();
    QString FI;
    if (q.next()) {
        FI= q.value(2).toByteArray().constData();
    }

    PFsA = FA.toInt();
    sigma_Fi = FI.toInt();

    PFA = PFsA * (0.65 + (0.01*sigma_Fi));

    if(PFsA!=0 && sigma_Fi!=0){
        ui->label_2->setText("PFA = " + FA + " x [0.65 +0.01 x " + FI + "]");
        ui->label_5->setText("PFA = " + FA + " x [0.65 +" + QString::number(0.01 * FI.toInt()) + "]");
        ui->label_6->setText("PFA = " + FA + " x [" + QString::number(0.65 + (0.01 * FI.toInt())) + "]");
        ui->lb_pfaResultado->setText("PFA = " + QString::number(PFA));

        if(PFA<=100){
            ui->lb_interpretacionComplejidad->setText("Interpretación de complejidad: Sencilla");
            complejidad = "Sencilla";
        }else if(PFA>100 && PFA<=300){
            ui->lb_interpretacionComplejidad->setText("Interpretación de complejidad: Media");
            complejidad = "Media";
        }else if(PFA>300){
            ui->lb_interpretacionComplejidad->setText("Interpretación de complejidad: Alta");
            complejidad = "Alta";
        }
    }else if(PFsA==0 || sigma_Fi==0){
        ui->label_2->setText("PFA = ???");
        ui->label_5->setText("<center>Debe llenarla tabla de conteo de complejidad y la tabla resumen Factor de Ajuste de Valor (FAV)</center>");
    }

    if(opc6==2){
        ModalidadEjemplo();
    }
}

Calculos::~Calculos()
{
    delete ui;
}

void Calculos::ModalidadEjemplo(){
    ui->spin_Personas->setValue(7);
    ui->dSpin_Horas->setValue(8);
    ui->dSpin_HorasMes->setValue(100);

    on_btn_Calcular_clicked();

    QFont f( "Times New Roman", 10, QFont::Bold);
    ui->lb_Demo->setFont( f);
    ui->lb_Demo->setText("<center>MODO DEMOSTRACIÓN (NO ES EDITABLE)</center> \nLos valores introducidos se usan ahora para calcular los puntos de"
                         " función ajustados. También  se puede agregar información adicional para calcular el número de horas por persona, "
                         " número de horas si se tiene un equipo de n personas y la duración del proyectos en meses.");

    ui->lb_Demo->setWordWrap(true);

    ui->spin_Personas->setEnabled(false);
    ui->dSpin_Horas->setEnabled(false);
    ui->dSpin_HorasMes->setEnabled(false);
    ui->btn_Calcular->setEnabled(false);
    ui->btn_Reporte->setStyleSheet("border: 1px solid rgb(0, 255, 0); border-width: 2px; border-radius: 10px; color: rgb(0,0,0);background-color: rgb(166, 166, 166);font-weight: bold;");

}

void Calculos::on_btn_Calcular_clicked()
{
    numPersonas = ui->spin_Personas->value();
    horasLaborales = ui->dSpin_Horas->value();
    horasLaboralesMes = ui->dSpin_HorasMes->value();

    if(PFsA==0 || sigma_Fi==0){
        QMessageBox::information(this, tr("¡ALERTA!"), tr("Para realizar los cálculos debe llenar la tabla de conteo de complejidad y\n"
                                                          "la tabla resumen Factor de Ajuste de Valor (FAV)"));
    }else if(numPersonas==0 || horasLaborales==0 || horasLaboralesMes==0){
        QMessageBox::information(this, tr("¡ALERTA!"), tr("Para realizar los cálculos debe rellenar todos los cambos primero."));
    }else{
        esfuerzo = PFA/(1/horasLaborales);
        ui->lb_esfuerzo->setText("PFA/(1/" + QString::number(horasLaborales)+") = " + QString::number(PFA) + "/" + QString::number(1/horasLaborales) + " = " + QString::number(esfuerzo) + " horas/persona");
        duracionHoras = esfuerzo/numPersonas;
        ui->lb_duracionHoras->setText("(" + QString::number(esfuerzo) + " horas/persona)/" + QString::number(numPersonas) + " = " + QString::number(duracionHoras) + " horas por miembro del equipo");
        duracionMeses = duracionHoras/horasLaboralesMes;
        ui->lb_duracionMes->setText(QString::number(duracionHoras) + " horas/(" + QString::number(horasLaboralesMes) + " horas/mes) = " + QString::number(duracionMeses) + " meses");
    }
}

void Calculos::on_btn_Menu_clicked()
{
    this->close(); //Se cierra esta ventana.

    Opciones *ops;
    ops = new Opciones(opc6, nombre_bd6, this);
    ops->show(); //Se abre la ventana para la simulación con varios objetos.
}

void Calculos::on_btn_Reporte_clicked()
{
    if(numPersonas==0 || horasLaborales==0 || horasLaboralesMes==0 || PFsA==0 || sigma_Fi==0 || esfuerzo==0){
        QMessageBox::information(this, tr("¡ALERTA!"), tr("Debe llenar todos los campos y realizar los cálculos para imprimir un reporte"));
    }else{

        QString fileName = "";
        QString date;
        fileName =QFileDialog::getSaveFileName(this,
                tr("Save Address Book"), "",
                tr("PDF - Formato de documento portátil (*.pdf);;All Files (*)"));

        if(fileName != ""){

            QDateTime now = QDateTime::currentDateTime();
            date = now.toString(QLatin1String("dddd / dd / MMMM / yyyy / hh:mm:ss AP"));

            QPdfWriter pdf(fileName);
            QPainter painter(&pdf);

            painter.setPen(Qt::black);
            QFont f("Times New Roman");

            painter.drawText(400, 300, "BD: " + nombre_bd6); //Impresión de la fecha.
            painter.drawText(6000, 300, date); //Impresión de la fecha.

            f.setBold(true);
            f.setPointSize(14);
            painter.setFont(f);

            painter.drawText(2000, 1000, "REPORTE DEL MÉTODO PUNTOS DE FUNCIÓN");

            painter.drawText(400, 1600, "Cálculo de los puntos de función ajustados: ");
            painter.drawText(400, 3200, "Interpretación de complejidad");
            painter.drawText(400, 4000, "Cálculo del esfuerzo horas/persona:");
            painter.drawText(400, 5200, "Duración del proyecto en horas (asignándolo a un equipo de " + QString::number(numPersonas) + "):");

            f.setUnderline(true);
            painter.setFont(f);

            painter.drawText(400, 6000, "Duración del proyecto en horas:");
            painter.drawText(400, 6800, "Duración en meses:");
            painter.drawText(400, 7600, "Duración del proyecto en meses:");

            f.setUnderline(false);
            f.setBold(false);
            painter.setFont(f);;

            painter.drawText(800, 2000, "Formula: PFA = PFsA x [0.65 +0.01 x Σ(fi)]");
            painter.drawText(800, 2400, "PFA =" + QString::number(PFsA) + "x [0.65 +0.01 x " + QString::number(sigma_Fi) + "]");
            painter.drawText(800, 2800, "PFA =" + QString::number(PFA));
            painter.drawText(800, 3600, complejidad);

            painter.drawText(800, 4400, "Suponiendo una jornada de " + QString::number(horasLaborales) + " horas de trabajo:");

            painter.drawText(800, 4800, "PFA/(1/" + QString::number(horasLaborales)+") = " + QString::number(PFA) + "/" + QString::number(1/horasLaborales) + " = " + QString::number(esfuerzo) + " horas/persona");

            painter.drawText(800, 5600, "(" + QString::number(esfuerzo) + " horas/persona)/" + QString::number(numPersonas) + " = " + QString::number(duracionHoras) + " horas por miembro del equipo");

            painter.drawText(800, 6400, QString::number(duracionHoras) + " horas por miembro del equipo");

            painter.drawText(800, 7200, QString::number(duracionHoras) + " horas/(" + QString::number(horasLaboralesMes) + " horas/mes) = " + QString::number(duracionMeses) + " meses");

            painter.drawText(800, 8000, QString::number(duracionMeses) + " meses con un equipo de desarrollo de " + QString::number(numPersonas) + " personas.");

            fileName = "";
        }
    }
}
