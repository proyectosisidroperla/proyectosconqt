#ifndef CALCULOS_H
#define CALCULOS_H

#include <QDialog>

#include <QDialog>
#include "QtSql/QSqlDatabase";
#include "QtSql/qsqlquery.h";
#include "QtSql/QSqlError";
#include "QtSql/QSqlQuery";

namespace Ui {
class Calculos;
}

class Calculos : public QDialog
{
    Q_OBJECT

public:
    explicit Calculos(int opc, QString nombre_bd, QWidget *parent = nullptr);
    ~Calculos();
    void ModalidadEjemplo();

private slots:

    void on_btn_Calcular_clicked();

    void on_btn_Menu_clicked();

    void on_btn_Reporte_clicked();

private:
    Ui::Calculos *ui;
    QSqlDatabase dbmain;
};

#endif // CALCULOS_H
