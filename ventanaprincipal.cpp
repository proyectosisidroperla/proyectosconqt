#include "ventanaprincipal.h"
#include "ui_ventanaprincipal.h"
#include <opciones.h>
#include <qinputdialog.h>
#include <QDesktopWidget>

VentanaPrincipal::VentanaPrincipal(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::VentanaPrincipal)
{
    ui->setupUi(this);

    setWindowFlags(windowFlags() | Qt::CustomizeWindowHint |
                                   Qt::WindowMinimizeButtonHint |
                                   Qt::WindowMaximizeButtonHint |
                                   Qt::WindowCloseButtonHint);
}

VentanaPrincipal::~VentanaPrincipal()
{
    delete ui;
}

void VentanaPrincipal::resizeEvent(QResizeEvent *evt)
{

    QPixmap bkgnd(":/new/prefix1/Imagenes/fondo.png");//Load picture.
    bkgnd = bkgnd.scaled(size(), Qt::IgnoreAspectRatio);//set scale of pic to match the window
    QPalette p = palette(); //copy current, not create new
    p.setBrush(QPalette::Background, bkgnd);//set the pic to the background
    setPalette(p);//show the background pic

    QMainWindow::resizeEvent(evt); //call base implementation

}

void VentanaPrincipal::on_pb_Manual_clicked()
{

    QStringList lst;
    lst << "Ejemplo" << "Espacio1" << "Espacio2" << "Espacio3" << "Espacio4" << "Espacio5";
    bool ok = false;
    QString res = QInputDialog::getItem(this, tr( "Bases de datos" ), tr( "Cargue un espacio para trabajar\nSe ha incluido un Ejemplo ya lleno" ), lst, 1, true, &ok);
    if ( ok ){
        this->close(); //Se cierra esta ventana.
        Opciones *opcs;
        opcs = new Opciones(1, res + ".sqlite", this);
        opcs->show(); //Se abre la ventana para la simulación con varios objetos.
    }else{
        res = "false";
    }

}

void VentanaPrincipal::on_pb_Demo_clicked()
{
    this->close(); //Se cierra esta ventana.

    Opciones *opcs;
    opcs = new Opciones(2, "BD_Ejemplo.sqlite", this);
    opcs->show(); //Se abre la ventana para la simulación con varios objetos.
}
