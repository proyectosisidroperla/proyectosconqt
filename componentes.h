#ifndef COMPONENTES_H
#define COMPONENTES_H

#include <QDialog>
#include "QtSql/QSqlDatabase";
#include "QtSql/qsqlquery.h";
#include "QtSql/QSqlError";
#include "QtSql/QSqlQuery";

namespace Ui {
class Componentes;
}

class Componentes : public QDialog
{
    Q_OBJECT

public:
    explicit Componentes(int opc, QString nombre_bd, QWidget *parent = nullptr);
    ~Componentes();
    void CrearTabla();
    void ModalidadEjemplo();

private slots:
    void on_btn_Agregar_clicked();

    void on_btn_siguientePaso_clicked();

    void on_btn_Menu_clicked();

    void on_tB_Info_clicked();

    void on_tB_Info_2_clicked();

private:
    Ui::Componentes *ui;
    QSqlDatabase dbmain;
};

#endif // COMPONENTES_H
