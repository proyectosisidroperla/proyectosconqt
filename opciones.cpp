#include "opciones.h"
#include "ui_opciones.h"
#include <componentes.h>
#include <factorajustedevalor.h>
#include <calculos.h>
#include <ventanaprincipal.h>

int opc1;
QString nombre_bd1;

Opciones::Opciones(int opc, QString nombre_bd, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Opciones)
{
    ui->setupUi(this);

    nombre_bd1 = nombre_bd;
    opc1 = opc;

    setWindowFlags(windowFlags() | Qt::CustomizeWindowHint |
                                   Qt::WindowMinimizeButtonHint |
                                   Qt::WindowMaximizeButtonHint |
                                   Qt::WindowCloseButtonHint);

    if(opc1==2){
        ModalidadEjemplo();
    }
}

Opciones::~Opciones()
{
    delete ui;
}

void Opciones::ModalidadEjemplo(){
    QFont f( "Times New Roman", 10, QFont::Bold);
    ui->lb_Demo->setFont( f);

    ui->lb_Demo->setText("<center>MODO DEMOSTRACIÓN (NO ES EDITABLE)</center> \nBienvenido, esto es un modo demostración de las funcionalidades del programa, los datos que se vayan"
                         " mostrando corresponden a un ejemplo precargado. Al final se calcularán los puntos de función ajustado correspondiente al programa.");
    ui->lb_Demo->setWordWrap(true);
}

void Opciones::on_btn_Componentes_clicked()
{
    this->close(); //Se cierra esta ventana.

    Componentes *comp;
    comp = new Componentes(opc1, nombre_bd1, this);
    comp->show(); //Se abre la ventana para la simulación con varios objetos.
}

void Opciones::on_btn_FAV_clicked()
{
    this->close(); //Se cierra esta ventana.

    FactorAjusteDeValor *fav;
    fav = new FactorAjusteDeValor(opc1, nombre_bd1, this);
    fav->show(); //Se abre la ventana para la simulación con varios objetos.
}

void Opciones::on_btn_Calculos_clicked()
{
    this->close(); //Se cierra esta ventana.

    Calculos *calc;
    calc = new Calculos(opc1, nombre_bd1, this);
    calc->show(); //Se abre la ventana para la simulación con varios objetos.
}

void Opciones::on_btn_Inicio_clicked()
{
    this->close(); //Se cierra esta ventana.

    VentanaPrincipal *vp;
    vp = new VentanaPrincipal(this);
    vp->show(); //Se abre la ventana para la simulación con varios objetos.
}
