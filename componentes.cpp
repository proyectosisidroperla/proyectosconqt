#include "componentes.h"
#include "ui_componentes.h"
#include "QDebug"
#include <complejidad.h>
#include <opciones.h>
#include <QMessageBox>

int opc2;
QString nombre_bd2;

QString descripcion;
QString componente;

Componentes::Componentes(int opc, QString nombre_bd, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Componentes)
{
    ui->setupUi(this);

    opc2 = opc;
    nombre_bd2 = nombre_bd;

    setWindowFlags(windowFlags() | Qt::CustomizeWindowHint |
                                   Qt::WindowMinimizeButtonHint |
                                   Qt::WindowMaximizeButtonHint |
                                   Qt::WindowCloseButtonHint);

    ui->cB_Componentes->addItems({"Entrada externa", "Salida externa", "Consulta externa", "Archivos lógicos internos", "Archivos de interfaz externos"}); //Elementos asignados al combobox

    dbmain = QSqlDatabase::addDatabase("QSQLITE");
    dbmain.setDatabaseName(nombre_bd2);
    if(dbmain.open()){
        qDebug() << "Base de datos creada";
    }else{
        qDebug() << "Error";
    }
    CrearTabla();

    if(opc2==2){
        ModalidadEjemplo();
    }
}

Componentes::~Componentes()
{
    delete ui;
}

void Componentes::ModalidadEjemplo(){

    QFont f( "Times New Roman", 12, QFont::Bold);
    ui->lb_Demo->setFont( f);

    ui->qE_Nombre->setText("Mensajes de confirmación");
    ui->cB_Componentes->setCurrentIndex(1);
    ui->btn_Agregar->setEnabled(false);

    ui->lb_Demo->setText("<center>MODO DEMOSTRACIÓN (NO ES EDITABLE)</center> \nPuede agregar nuevos componentes escribiéndolos en la caja de texto y seleccionando"
                         " abajo su categoría respectiva.");
    ui->lb_Demo->setWordWrap(true);

    ui->qE_Nombre->setEnabled(false);

    ui->btn_siguientePaso->setStyleSheet("border: 1px solid rgb(255, 0, 0); border-width: 2px; border-radius: 10px; color: rgb(0,0,0);background-color: rgb(166, 166, 166);font-weight: bold;");
}

void Componentes::CrearTabla(){ //Creación de la tabla
    QString consulta;
    consulta.append("CREATE TABLE IF NOT EXISTS componentes("
                    "id INTEGER PRIMARY KEY AUTOINCREMENT,"
                    "Componente VARCHAR(30),"
                    "Descripcion VARCHAR (50),"
                    "Complejidad VARCHAR (50)"

                    ");");

    QSqlQuery crear;
    crear.prepare(consulta);
    if(crear.exec()){
        qDebug() << "Tabla creada";
    }else{
        qDebug() << "Tabla no creada" << crear.lastError();
    }
}

void Componentes::on_btn_Agregar_clicked()
{
    descripcion = ui->qE_Nombre->text();
    componente = ui->cB_Componentes->currentText();

    if(descripcion.isEmpty()){
        QMessageBox::information(this, tr("¡AYUDA!"), tr("Por favor debe llenar la descripción del componente\n"
                                                         "para poder agregarlo."));
    }else{

        QSqlQuery insertar_db;
        insertar_db.prepare("INSERT INTO componentes(Componente,Descripcion,Complejidad)"
                            "VALUES (:Componente,:Descripcion,:Complejidad)");

        insertar_db.bindValue(":Componente", componente);
        insertar_db.bindValue(":Descripcion", descripcion);
        insertar_db.bindValue(":Complejidad", "");

        if(insertar_db.exec()){
            qDebug() << "Datos ingresados a la tabla";
        }else{
            qDebug() << "Error al ingresar los datos" << insertar_db.lastError();
        }

        QMessageBox::information(this, tr("¡MENSAJE!"), tr("Componente agregado exitosamente."));

        ui->qE_Nombre->clear();
    }
}

void Componentes::on_btn_siguientePaso_clicked()
{
    this->close(); //Se cierra esta ventana.

    Complejidad *cmpl;
    cmpl = new Complejidad(opc2, nombre_bd2, this);
    cmpl->show(); //Se abre la ventana para la simulación con varios objetos.|
}

void Componentes::on_btn_Menu_clicked()
{
    this->close(); //Se cierra esta ventana.

    Opciones *ops;
    ops = new Opciones(opc2, nombre_bd2, this);
    ops->show(); //Se abre la ventana para la simulación con varios objetos.|
}

void Componentes::on_tB_Info_clicked()
{
    QMessageBox::information(this, tr("¡AYUDA!"), tr("Aquí puede introducir diferentes componentes\n"
                                                     "según la cetogoría que desee"));
}

void Componentes::on_tB_Info_2_clicked()
{
    QMessageBox::information(this, tr("¡AYUDA!"), tr("Debe elegir una categoría de la lista"
                                                     " para ingresar los componentes"));
}
