#ifndef COMPLEJIDAD_H
#define COMPLEJIDAD_H

#include <QDialog>

#include <QDialog>
#include "QtSql/QSqlDatabase";
#include "QtSql/qsqlquery.h";
#include "QtSql/QSqlError";
#include "QtSql/QSqlQuery";

namespace Ui {
class Complejidad;
}

class Complejidad : public QDialog
{
    Q_OBJECT

public:
    explicit Complejidad(int opc, QString nombre_bd, QWidget *parent = nullptr);
    ~Complejidad();
    void MostrarDatos();
    void ModalidadEjemplo();

private slots:
    void on_cB_Componentes_currentIndexChanged(int index);

    void on_tW_Datos_cellClicked(int row, int column);

    void OnComboIndexChanged(const QString& text);

    void on_btn_siguientePaso_clicked();

    void on_bnt_Menu_clicked();

    void on_btn_pasoAnterior_clicked();

    void on_tB_Info_clicked();

private:
    Ui::Complejidad *ui;
    QSqlDatabase dbmain;
};

#endif // COMPLEJIDAD_H
